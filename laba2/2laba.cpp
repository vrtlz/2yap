#include <iostream>

using std::cin;
using std::cout;

class chisla {
private:
    int x;
    int y;

public:
    void set(int x, int y) {
        this->x = x;
        this->y = y;
    }

    int get_x() const {
        return x;
    }

    int get_y() const {
        return y;
    }

    int get_sub() const {
        return x - y;
    }
    double get_arithmetic_mean_of_the_cubes() const {
        return (x * x * x + y * y * y) * 0.5;
    }
};


int main() {
    chisla A;
    int x, y;
    cout << "Введите значение x\n";
    cin >> x;
    cout << "Введите значение y\n";
    cin >> y;
    A.set(x, y);

    cout << "x = " << A.get_x() << "\n";
    cout << "y = " << A.get_y() << "\n";
    cout << "x - y = " << A.get_sub() << "\n";
    cout << "(x^3 + y^3) / 2 = " << A.get_arithmetic_mean_of_the_cubes();
    return 0;
}