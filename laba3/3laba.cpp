#include <fstream>
#include <iostream>

using namespace std;

class sort {
private:
    int size_of_array;
    int *array;

public:
    void readFile(ifstream &file) {
        if (!file.is_open()) {
            return;
        }

        file >> size_of_array;
        array = new int[size_of_array];

        for (int i = 0; i < size_of_array; i++) {
            file >> array[i];
        }
    }

    //сотировка вставками
    void insertionSort() {
        for (int i = 1; i < size_of_array; i++) {
            int a = array[i];
            for (int j = i - 1; (j >= 0) && (a < array[j]); j--) {
                array[j + 1] = array[j];
                array[j] = a;
            }
        }
    }

    //пузырьковая сортировка
    void bubbleSort() {
        for (int i = 1; i < size_of_array; i++)
            for (int j = size_of_array - 1; j >= i; j--) {
                if (array[j - 1] > array[j]) {
                    int a = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = a;
                }
            }
    }

    void printArray() {
        cout << "Your array: \n";
        for (int i = 0; i < size_of_array; i++) {
            cout << array[i] << " ";
        }
        cout << endl << endl;
    }
};


int main() {
    ifstream file("../input.txt");
    if (!file.is_open()) {
        return 1;
    }

    sort A;

    A.readFile(file);
    A.printArray();
    A.insertionSort();
    A.printArray();

    file.seekg(0);
    A.readFile(file);
    A.printArray();
    A.bubbleSort();
    A.printArray();

    return 0;
}