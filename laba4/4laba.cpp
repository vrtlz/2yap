#include <iostream>

class numbers {
private:
    int a;
    int b;

public:
    void printNumber() const {
        std::cout << "\n" << a << " + " << b << "*sqrt(3)\n";
    }

    numbers operator*(const numbers &num) const {
        int value1 = a * num.a + 3 * b * num.b;
        int value2 = a * num.b + b * num.a;
        numbers result(value1, value2);
        return result;
    }

    numbers() = default;

    numbers(int a, int b) {
        this->a = a;
        this->b = b;
    }
};

int main() {
    int a1, b1, a2, b2;
    std::cout << "Enter a for the first number : ";
    std::cin >> a1;
    std::cout << "Enter b for the first number : ";
    std::cin >> b1;

    std::cout << "Enter a for the second number : ";
    std::cin >> a2;
    std::cout << "Enter b for the second number : ";
    std::cin >> b2;

    numbers num1(a1, b1);
    numbers num2(a2, b2);

    numbers num3 = num1 * num2;
    
    num3.printNumber();

    return 0;
}
