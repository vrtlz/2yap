#pragma once

#include <iostream>

class List {
private:
    struct Node {
        int a;
        int b;
        Node *next;
    };
    Node *start;
    Node *p;

public:
    List(int x = 0, int y = 0) {
        Node *nodeEl = new Node;
        nodeEl->a = x;
        nodeEl->b = y;
        start = nodeEl;
        p = nodeEl;
    }

    ~List() {
        Node *current = start;
        while (current != nullptr) {
            Node *next_current = current->next;
            delete current;
            current = next_current;
        }
    }

    void addNext(int x, int y) {
        Node *newEl = new Node;
        newEl->a = x;
        newEl->b = y;
        newEl->next = p->next;
        p->next = newEl;
    }

    void rmElement() {
        Node *current = start;
        while (current->next != p) {
            current = current->next;
        }
        current->next = p->next;
        delete p;
        p = current;
    }

    void showElement() {
        std::cout << "(" << p->a << ", " << p->b << ") \n";
    }

    void changeElement(int x, int y) {
        p->a = x;
        p->b = y;
    }

    void toStart() {
        p = start;
    }

    void toNext() {
        if (p->next != nullptr)
            p = p->next;
    }

    void showList() {
        Node *current = start;
        while (current != nullptr) {
            if (current != p) {
                std::cout << "(" << current->a << ", " << current->b << ") \n";
            } else {
                std::cout << "[(" << current->a << ", " << current->b << ")] \n";
            }
            current = current->next;
        }
    }

    void toSpecialElement() {
        Node *current = start;
        Node *this_element = nullptr;
        while (current != nullptr) {
            if (current->a * current->b == 0) {
                this_element = current;
            }
            current = current->next;
        }
        if (this_element == nullptr) {
            std::cout << "Элементов, для которых a * b = 0, в списке нет\n";
        } else {
            p = this_element;
        }
    }
};