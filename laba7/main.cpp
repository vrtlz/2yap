#include "list.h"

using namespace std;

int main() {
    int a, b;
    std::cout << "Введите два целых числа через пробел для первого элемента списка: ";
    std::cin >> a >> b;

    List *list = new List(a, b);

    int choice = 0;
    while (choice != 9) {
        cout << "\n1) Добавить элемент после элемента, на который настроен указатель p (после добавления "
                "указатель p остается на месте)\n"
                "2) Удалить элемент, на который настроен указатель p (после удаления указатель p будет "
                "настроен на элемент, предшествующий удаляемому)\n"
                "3) Показать значения полей данных элемента, на который настроен указатель p\n"
                "4) Поменять значения полей данных элемента, на который настроен указатель p\n"
                "5) Переместить указатель p в начало списка\n"
                "6) Переместить указатель p на один элемент вправо\n"
                "7) Вывести на консоль значение всех элементов списка\n"
                "8) Найти последний элемент списка (настроить на него указатель p) для которого выполняется "
                "равенство a*b=0\n"
                "9) Выход из меню\n";

        cin >> choice;

        int x, y;
        switch (choice) {
            case 1:
                cout << "Введите через пробел a и b: ";
                cin >> x >> y;
                list->addNext(x, y);
                break;
            case 2:
                list->rmElement();
                break;
            case 3:
                list->showElement();
                break;
            case 4:
                cout << "Введите через пробел a и b: ";
                cin >> x >> y;
                list->changeElement(x, y);
                break;
            case 5:
                list->toStart();
                break;
            case 6:
                list->toNext();
                break;
            case 7:
                list->showList();
                break;
            case 8:
                list->toSpecialElement();
                break;
            case 9:
                cout << "Выход...";
                list->~List();
                break;
            default:
                cout << "Вы ввели неккоректное значение\n";
                break;
        }
    }

    return 0;
}
