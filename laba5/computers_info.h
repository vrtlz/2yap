#include <utility>

#pragma once

using namespace std;

class Computer {
protected:
    string Firm;
    string Mark;
    int YearB;

public:
    Computer() = default;

    Computer(string Firm, string Mark, int YearB) {
        this->Firm = move(Firm);
        this->Mark = move(Mark);
        this->YearB = YearB;
    }

    string Print() {
        return Firm + " " + Mark + " " + to_string(YearB) + "\n";
    }

    void ChangeF(string firm) {
        Firm = move(firm);
    }

    void ChangeM(string mark) {
        Mark = move(mark);
    }

    void ChangeY(int yearB) {
        YearB = yearB;
    }
};

class Desktop : public Computer {
private:
    int Power;

public:
    Desktop() = default;

    Desktop(string Firm, string Mark, int YearB, int power) : Computer(move(Firm), move(Mark), YearB), Power(power) {}

    void ChangeP(int power) {
        Power = power;
    }

    string Print(int year) {
        if (year < YearB) {
            return Firm + " " + Mark + " Еще не выпущен " + to_string(Power) + "\n";
        } else if (year - YearB > 5) {
            return Firm + " " + Mark + " Устарел " + to_string(Power) + "\n";
        } else {
            return Firm + " " + Mark + " " + to_string(year - YearB) + " " + to_string(Power) + "\n";
        }
    }
};

class Laptop : public Computer {
private:
    int Core;

public:
    Laptop() = default;

    Laptop(string Firm, string Mark, int YearB, int core) : Computer(move(Firm), move(Mark), YearB), Core(core) {}

    void ChangeC(int core) {
        Core = core;
    }

    string Print(int year) {
        if (year < YearB) {
            return Firm + " " + Mark + " Еще не выпущен " + to_string(Core)  + "\n";
        } else if (year - YearB > 5) {
            return Firm + " " + Mark + " Требуется чистка " + to_string(Core)  + "\n";
        } else {
            return Firm + " " + Mark + " " + to_string(year - YearB) + " " + to_string(Core) + "\n";
        }
    }
};
