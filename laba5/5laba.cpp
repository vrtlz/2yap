#include <iostream>
#include "computers_info.h"
#include <fstream>

int main() {
    ifstream file("../input.txt");
    if (!file.is_open()) {
        return 1;
    }

    int year_now;
    file >> year_now;

    int system_unit_count = 0, laptop_count = 0;

    while (!file.eof()) {
        char n;
        string firm, mark;
        int year, more_info;
        file >> n >> firm >> mark >> year >> more_info;
        if (n == 'C') {
            system_unit_count++;
        }
        if (n == 'H') {
            laptop_count++;
        }
    }

    auto *computer_array = new Computer[system_unit_count + laptop_count];
    auto *desktop_array = new Desktop[system_unit_count];
    auto *laptop_array = new Laptop[laptop_count];

    file.seekg(0);
    file >> year_now;

    int position_Com = 0;
    int position_Des = 0;
    int position_Lap = 0;
    while (!file.eof()) {
        char n;
        string firm, mark;
        int year, more_info;
        file >> n >> firm >> mark >> year >> more_info;
        computer_array[position_Com].ChangeF(firm);
        computer_array[position_Com].ChangeM(mark);
        computer_array[position_Com].ChangeY(year);
        position_Com++;

        if (n == 'C') {
            desktop_array[position_Des].ChangeF(firm);
            desktop_array[position_Des].ChangeM(mark);
            desktop_array[position_Des].ChangeY(year);
            desktop_array[position_Des].ChangeP(more_info);
            position_Des++;
        }
        if (n == 'H') {
            laptop_array[position_Lap].ChangeF(firm);
            laptop_array[position_Lap].ChangeM(mark);
            laptop_array[position_Lap].ChangeY(year);
            laptop_array[position_Lap].ChangeC(more_info);
            position_Lap++;
        }

    }

    ofstream desktopfile("../Desktop.txt");
    ofstream laptopfile("../Laptop.txt");
    ofstream computerfile("../Computer.txt");

    for (int i = 0; i < position_Des; i++) {
        desktopfile << desktop_array[i].Print(year_now);
    }

    for (int i = 0; i < position_Lap; i++) {
        laptopfile << laptop_array[i].Print(year_now);
    }

    for (int i = 0; i < position_Com; i++) {
        computerfile << computer_array[i].Print();
    }

    return 0;
}

