#include <iostream>

const size_t SIZE = 50;

void print_stack(int[], int count);

void push(int[], int *count, int);

int add_and_pop(int[], int *count);

int main() {
    int stack[SIZE];
    int count = 0;
    int key = 0, element = 0;

    while (key != 3) {
        printf("\nСодержимое стека: \n");
        print_stack(stack, count);
        printf("1.Добавить элемент в стек.\n");
        printf("2.Сложить два верхних элемента стека и извлечь результат. Верхний элемент\n"
               "стека удаляется. \n");
        printf("3.Выход из программы.\n");
        printf("Выберите операцию: ");
        scanf("%i", &key);
        if (key == 1) {
            printf("Введите значение элемента: ");
            scanf("%i", &element);
            push(stack, &count, element);
        }
        if (key == 2) {
            if (count < 2) {
                printf("Элементов в стеке недостаточно. \n ");
            } else {
                printf("Сумма двух верхних элементов: %i. Верхний элемент удален.\n", add_and_pop(stack, &count));
            }
        }
    }

    return 0;


void print_stack(int stack[], int count) {
    if (count == 0) {
        printf("стек пуст\n\n");
        return;
    } else if (count > SIZE || count < 0) {
        printf("Неккоректное количество элементов\n");
    } else {
        for (int i = 0; i < count; i++) {
            printf("%i", stack[i]);
            printf("\n");
        }
    }
}

void push(int stack[], int *count, int element) {
    if (*count == 50) {
        printf("Стек полон. Добавление элемента невозможно.");
    } else {
        if (*count == 0) {
            stack[0] = element;
            (*count)++;
        } else {
            for (int i = *count; i > 0; i--) stack[i] = stack[i - 1];
            stack[0] = element;
            (*count)++;
        }
    }
}

int add_and_pop(int stack[], int *count) {
    int element1 = stack[0];
    int element2 = stack[1];
    for (int i = 0; i < *stack; i++) stack[i] = stack[i + 1];
    (*count)--;
    return element1 + element2;
}
